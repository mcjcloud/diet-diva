import RPi.GPIO as GPIO
import time
import os
import random
from aiy.voice.audio import AudioFormat, play_wav

#GPIO Mode (BOARD / BCM)
GPIO.setmode(GPIO.BCM)
 
#set GPIO Pins
GPIO_TRIGGER = 26
GPIO_ECHO = 6
 
#set GPIO direction (IN / OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)

def randomFile():
  files = os.listdir("fatshaming")
  i = random.randint(0, len(files) - 1)
  return "./fatshaming/" + files[i]
 
def distance():
  # set Trigger to HIGH
  GPIO.output(GPIO_TRIGGER, True)

  # set Trigger after 0.01ms to LOW
  time.sleep(0.00001)
  GPIO.output(GPIO_TRIGGER, False)

  StartTime = time.time()
  StopTime = time.time()

  # save StartTime
  while GPIO.input(GPIO_ECHO) == 0:
    StartTime = time.time()

  # save time of arrival
  while GPIO.input(GPIO_ECHO) == 1:
    StopTime = time.time()

  # time difference between start and arrival
  TimeElapsed = StopTime - StartTime
  # multiply with the sonic speed (34300 cm/s)
  # and divide by 2, because there and back
  distance = (TimeElapsed * 34300) / 2

  return distance

def main():
  try:
    # play_wav("./fatshaming/hes-fat.wav")
    counter = 0
    open = False
    while True:
      dist = distance()
      if dist > 40 and not open:
        if counter > 5:
          open = True
          counter = 0
          print ("Door open")
          fileName = randomFile()
          print ("Playing audio " + fileName)
          play_wav(fileName)
          print ("Done.")
        else:
          counter += 1
      elif dist <= 40 and open:
        if counter > 5:
          open = False
          counter = 0
          print ("Door shut")
        else:
          counter += 1
      # print ("Measured Distance = %.1f cm" % dist)
      time.sleep(0.05)

  # Reset by pressing CTRL + C
  except KeyboardInterrupt:
    print("Measurement stopped by User")
    GPIO.cleanup()

if __name__ == '__main__':
  main()
